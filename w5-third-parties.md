# Third Parties: IDP Connect

## Facebook

First, go to the `Identity Providers` left menu item and select Facebook from the `Add provider` drop down list. This will bring you to the `Add identity provider` page.
Add Identity Provider

![image](images/facebook-add-identity-provider.png)

You can’t click save yet, as you’ll need to obtain a `Client ID` and `Client Secret` from Facebook. One piece of data you’ll need from this page is the `Redirect URI`. You’ll have to provide that to Facebook when you register Keycloak as a client there, so copy this URI to your clipboard.

Once you’ve logged into Facebook Developer Console there is a pull down menu in the top right corner of the screen that says My Apps. Select the Add a New App menu item.
Add a New App

![image](images/facebook-add-new-app.png)

Select the Website icon. Click the Skip and Create App ID button.
Create a New App ID

![image](images/facebook-create-app-id.png)


The email address and app category are required fields. Once you’re done with that, you will be brought to the dashboard for the application. Click the Settings left menu item.
Create a New App ID

![image](images/facebook-app-settings.png)

Click on the + Add Platform button at the end of this page and select the Website icon. Copy and paste the Redirect URI from the Keycloak Add identity provider page into the Site URL of the Facebook Website settings block.
Specify Website

![image](images/facebook-app-settings-website.png)



After this it is necessary to make the Facebook app public. Click App Review left menu item and switch button to "Yes".

You will need also to obtain the App ID and App Secret from this page so you can enter them into the Keycloak Add identity provider page. To obtain this click on the Dashboard left menu item and click on Show under App Secret. Go back to Keycloak and specify those items and finally save your Facebook Identity Provider.

One config option to note on the Add identity provider page for Facebook is the Default Scopes field. This field allows you to manually specify the scopes that users must authorize when authenticating with this provider. By default, Keycloak uses the following scopes: email. For a complete list of scopes, please take a look at https://developers.facebook.com/docs/graph-api. 
