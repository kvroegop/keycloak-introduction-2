# Role management

This task is all about giving users permission to use certain endpoints and function. In keycloak we can define roles to separate access to functionality only to those who need it and have permission. Those roles can then be assigned to a user. In this task we will create roles to protect the read and write permissions for our desk application.

# Keycloak config

* In keycloak go to the realm workshop and select roles in the side menu.
* Click on "Add Role" in the top right of the table.
* Create a role named: "desk_read" and "desk_write".
* Select the user you want to give some roles to. you can find the users under manage/users in the side menu. (click on the id of edit to change the users values)
* Go to "Role Mappings in the top menu"
* Select "desk_write" and click on add selected.

# Backend config

Go to the file: `DeskController.java` (backend\src\main\java\com\workshop\keycloak\backend\controller\DeskController.java) remove all comments around the Keycloak code to secure the endpoint with a role.

# Frontend config

* Go to file: `Desk.vue` (frontend\src\views\Desks.vue) and remove all comments.
* Go to file: `Navbar.vue` (frontend\src\components\Navbar.vue) and remove all comments.

Now you have successfully secured your application with roles. You cannot see the details page of a desk.

Try it for yourself: Give the user the correct role to see the details page.

# Testing

After all these changes in the frontend, is the base code and the production code out of sync therefore we need to run the command: `mvn package` in the root folder of the project. This command packages the vue files and places them inside the proper folder in the backend so the backend can serve the new production ready frontend.